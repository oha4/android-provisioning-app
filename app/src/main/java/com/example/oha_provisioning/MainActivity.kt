package com.example.oha_provisioning

import com.example.oha_provisioning.ui.theme.TestTheme

import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothManager
import android.bluetooth.le.ScanCallback
import android.bluetooth.le.ScanResult
import android.content.Context
import android.content.pm.PackageManager
import android.os.Bundle
import android.Manifest
import android.bluetooth.BluetoothDevice
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Edit
import androidx.compose.material.icons.filled.Refresh
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.LocalContentColor
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.navigation.NavController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController


class MainActivity : ComponentActivity() {
    private var scannedDevices by mutableStateOf(emptyList<BluetoothDevice>())
    private val bluetoothViewModel: BluetoothViewModel by viewModels()

    companion object {
        private const val BLUETOOTH_SCAN_CODE = 100
        private const val BLUETOOTH_CONNECT_CODE = 101
        private const val BLUETOOTH_FINE_CODE = 102
        private const val BLUETOOTH_COARSE_CODE = 103
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContent {
            TestTheme {
                val navController = rememberNavController()

                NavHost(navController = navController, startDestination = "home") {
                    composable("home") {
                        HomeScreen(navController)
                    }
                    composable("details") {
                        DetailsScreen(navController, bluetoothViewModel)
                    }
                }

            }
        }
    }

    @OptIn(ExperimentalMaterial3Api::class)
    @Composable
    fun HomeScreen(navController: NavController) {

        TestTheme {
            Surface(
                modifier = Modifier.fillMaxSize(),
                color = MaterialTheme.colorScheme.background
            ) {
                Column {
                    // Initialize BluetoothManager
                    val bluetoothManager =
                        getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager

                    // Get BluetoothAdapter from BluetoothManager
                    val bluetoothAdapter: BluetoothAdapter? = bluetoothManager.adapter

                    // Check if Bluetooth is supported on the device
                    if (bluetoothAdapter == null) {
                        println("Bluetooth is not supported on this device.")
                        return@Surface
                    }

                    checkBluetoothPermissions()
                    startBluetoothDeviceScan(bluetoothAdapter)

                    TopAppBar(
                        title = {
                            Row(
                                verticalAlignment = Alignment.CenterVertically,
                                horizontalArrangement = Arrangement.spacedBy(8.dp)
                            ) {
                                Text(text = "Scanned Devices", style = MaterialTheme.typography.headlineMedium)
                                Spacer(modifier = Modifier.weight(1f))
                                IconButton(
                                    onClick = {
                                        scannedDevices = emptyList()
                                        startBluetoothDeviceScan(bluetoothAdapter)
                                    },
                                    modifier = Modifier.weight(1f)
                                ) {
                                    Icon(
                                        Icons.Default.Refresh,
                                        contentDescription = null,
                                        tint = LocalContentColor.current
                                    )
                                }
                            }
                        },
                        colors = TopAppBarDefaults.topAppBarColors(
                            containerColor = MaterialTheme.colorScheme.primary,
                            titleContentColor = MaterialTheme.colorScheme.onPrimary,
                        ),
                    )
                    ScannedDeviceList(scannedDevices) { selectedDevice ->
                        bluetoothViewModel.selectedDevice = selectedDevice
                        navController.navigate("details")
                    }
                }
            }
        }
    }


    private fun checkBluetoothPermissions() {
        // Location permission is not granted, request it
        checkPermission(Manifest.permission.BLUETOOTH_SCAN, BLUETOOTH_SCAN_CODE)
        checkPermission(Manifest.permission.BLUETOOTH_CONNECT, BLUETOOTH_CONNECT_CODE)
        checkPermission(Manifest.permission.ACCESS_FINE_LOCATION, BLUETOOTH_FINE_CODE)
        checkPermission(Manifest.permission.ACCESS_COARSE_LOCATION, BLUETOOTH_COARSE_CODE)
    }

    // Function to check and request permission.
    private fun checkPermission(permission: String, requestCode: Int) {
        if (ContextCompat.checkSelfPermission(this@MainActivity, permission) == PackageManager.PERMISSION_DENIED) {

            // Requesting the permission
            ActivityCompat.requestPermissions(this@MainActivity, arrayOf(permission), requestCode)
        }
    }

    // This function is called when the user accepts or decline the permission.
    // Request Code is used to check which permission called this function.
    // This request code is provided when the user is prompt for permission.
    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>,
                                            grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this@MainActivity, "Permission Granted", Toast.LENGTH_SHORT).show()
            } else {
                Toast.makeText(this@MainActivity, "Permission Denied", Toast.LENGTH_SHORT).show()
            }
    }


    private fun startBluetoothDeviceScan(bluetoothAdapter: BluetoothAdapter) {
        val bluetoothLeScanner = bluetoothAdapter.bluetoothLeScanner

        val scanCallback = object : ScanCallback() {
            override fun onScanResult(callbackType: Int, result: ScanResult?) {
                super.onScanResult(callbackType, result)

                // Check if the scanned device is the one you are looking for
                result?.device?.let { bluetoothDevice ->
                    // Update the list of scanned devices
                    if ( bluetoothDevice != null && !scannedDevices.contains(bluetoothDevice) && bluetoothDevice.name != null){
                        Log.d("Bluetooth", "Found new Device \"${bluetoothDevice.name}\"")
                        scannedDevices = scannedDevices + bluetoothDevice
                    }
                }
            }

            override fun onScanFailed(errorCode: Int) {
                super.onScanFailed(errorCode)
                println("Bluetooth Scan failed with error code: $errorCode")
            }
        }

        // Start Bluetooth device scan
        bluetoothLeScanner?.startScan(scanCallback)

        // Handler für die Verzögerung von 30 Sekunden erstellen
        val handler = Handler(Looper.getMainLooper())
        handler.postDelayed({
            // Stop den Bluetooth-Scan nach 30 Sekunden
            bluetoothLeScanner?.stopScan(scanCallback)
        }, 30000) // 30 Sekunden in Millisekunden umgerechne
    }


    @Composable
    fun ScannedDeviceList(devices: List<BluetoothDevice>, onItemClick: (BluetoothDevice) -> Unit) {
        LazyColumn {
            items(devices) { device ->
                ScannedDevice(device, onItemClick)
            }
        }


    }

    @Composable
    fun ScannedDevice(device: BluetoothDevice, onItemClick: (BluetoothDevice) -> Unit) {

        Card(
            modifier = Modifier
                .fillMaxWidth()
                .padding(5.dp)
                .clickable {
                    onItemClick(device)
                },
            colors = CardDefaults.cardColors(containerColor = MaterialTheme.colorScheme.primaryContainer)
        ) {
            Row(
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.spacedBy(8.dp),
                modifier = Modifier.padding(all = 5.dp)
            ){
                Icon(
                    Icons.Default.Edit,
                    contentDescription = null,
                    tint = LocalContentColor.current
                )
                Column(
                    modifier = Modifier
                        .weight(1f)
                        .padding(5.dp),
                    verticalArrangement = Arrangement.Center,)
                {
                    Text(text = "${device.name}", style = MaterialTheme.typography.titleLarge)
                    Text(text = "${device.address}", style = MaterialTheme.typography.bodyMedium)
                }
            }
        }
    }
}






