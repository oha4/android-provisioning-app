package com.example.oha_provisioning

import android.bluetooth.BluetoothDevice
import androidx.lifecycle.ViewModel

class BluetoothViewModel : ViewModel() {
    var selectedDevice: BluetoothDevice? = null
}
