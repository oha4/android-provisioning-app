package com.example.oha_provisioning

import android.bluetooth.BluetoothDevice
import android.content.Intent
import android.util.Log
import android.widget.Toast
import android.net.Uri
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.filled.ArrowBack
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.Button
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.HorizontalDivider
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.LinearProgressIndicator
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.MutableFloatState
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableFloatStateOf
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import com.example.oha_provisioning.ui.theme.TestTheme
import io.runtime.mcumgr.McuMgrCallback
import io.runtime.mcumgr.ble.McuMgrBleTransport
import io.runtime.mcumgr.dfu.FirmwareUpgradeCallback
import io.runtime.mcumgr.dfu.FirmwareUpgradeController
import io.runtime.mcumgr.dfu.FirmwareUpgradeManager
import io.runtime.mcumgr.exception.McuMgrException
import io.runtime.mcumgr.image.McuMgrImage
import io.runtime.mcumgr.managers.ImageManager
import io.runtime.mcumgr.managers.SettingsManager
import io.runtime.mcumgr.response.McuMgrResponse
import io.runtime.mcumgr.response.img.McuMgrImageStateResponse
import io.runtime.mcumgr.response.settings.McuMgrSettingsReadResponse
import io.runtime.mcumgr.transfer.UploadCallback
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.launch
import java.io.ByteArrayOutputStream
import java.io.InputStream
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine

enum class FwupState{
    NOT_SARTED,
    UPLOADING,
    REBOOTING,
    FINISHED,
    FAILED
}

@Composable
fun showUploadDialog(state: MutableState<FwupState>, progress: MutableFloatState, dfu: FirmwareUpgradeManager?){
    AlertDialog(
        onDismissRequest = {},
        confirmButton =
        {
            TextButton(
                onClick = {
                    state.value = FwupState.NOT_SARTED
                },
                enabled = if(state.value == FwupState.FINISHED) true else false
            ){
                Text("Abgeschlossen")
            }
        },
        title = {Text("Firmware Update", style = MaterialTheme.typography.headlineMedium)},
        dismissButton =
        {
            TextButton(
                onClick = {
                    dfu?.cancel()
                    state.value = FwupState.NOT_SARTED
                }
            ) {
                Text("Abbrechen")
            }
        },
        text = {
            Box{
                Column(modifier = Modifier
                    .fillMaxWidth()
                    .padding(all = 5.dp),
                    verticalArrangement = Arrangement.Center,
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    CircularProgressIndicator(
                        progress = { progress.floatValue },
                    )
                    when(state.value){
                        FwupState.UPLOADING -> {
                            Text(text = "Upload Image")
                        }

                        FwupState.REBOOTING -> {
                            Text(text = "Rebooting Device")
                        }

                        FwupState.FINISHED -> {
                            Text(text = "Update finished successfully")
                        }

                        FwupState.FAILED -> {
                            Text(text = "Upgrade Failed")
                        }

                        FwupState.NOT_SARTED -> {}
                    }
                }
            }
        }
    )
}

@Preview
@Composable
fun showUploadDialogUploadingPreview(){
    val state = remember { mutableStateOf(FwupState.UPLOADING)}
    val progress = remember { mutableFloatStateOf(0.75f)}
    val dfu: FirmwareUpgradeManager? = null

    TestTheme {
        showUploadDialog(state = state, progress = progress, dfu = dfu)
    }
}

@Preview
@Composable
fun showUploadDialogrebootingPreview(){
    val state = remember { mutableStateOf(FwupState.REBOOTING)}
    val progress = remember { mutableFloatStateOf(1.0f)}
    val dfu: FirmwareUpgradeManager? = null

    TestTheme {
        showUploadDialog(state = state, progress = progress, dfu = dfu)
    }
}

@Preview
@Composable
fun showUploadDialogFinishedPreview(){
    val state = remember { mutableStateOf(FwupState.FINISHED)}
    val progress = remember { mutableFloatStateOf(1.0f)}
    val dfu: FirmwareUpgradeManager? = null

    TestTheme {
        showUploadDialog(state = state, progress = progress, dfu = dfu)
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun DetailsScreen(navController: NavController, bluetoothViewModel: BluetoothViewModel) {
    val device = bluetoothViewModel.selectedDevice ?: return
    val context = LocalContext.current
    var isLoading = remember { mutableStateOf<Boolean>(true) }
    var devName = remember { mutableStateOf<String?>(null) }
    var devManufacturer = remember { mutableStateOf<String?>(null) }
    var devHardwareVersion = remember { mutableStateOf<String?>(null) }
    var ssidState = remember { mutableStateOf<String?>(null) }
    var passwordState = remember { mutableStateOf<String?>(null) }
    var serverState = remember { mutableStateOf<String?>(null) }
    var techState = remember { mutableStateOf<UInt>(0u) }
    var image = remember { mutableStateOf<McuMgrImageStateResponse.ImageSlot?>(null) }
    var updateFile  = remember { mutableStateOf<Uri?>(null) }
    var updateFileVersion = remember { mutableStateOf<String?>(null) }
    var uploadButtonEnabled = remember { mutableStateOf(false) }

    val transport = McuMgrBleTransport(context, device)
    val settingsManager = SettingsManager(transport)
    val imageManager = ImageManager(transport)


    fun ByteArray.toUInt(): UInt {
        var result = 0u
        for (i in this.indices) {
            result += (this[i].toUInt() shl 24) shr (24-i*8) ;
        }
        return result
    }

    fun ByteArray.toHex(): String {
        val hexChars = "0123456789ABCDEF"
        val result = StringBuilder()
        for (byte in this) {
            val hexByte = (byte.toInt() and 0xFF)
            result.append(hexChars[hexByte shr 4]).append(hexChars[hexByte and 0x0F]).append(":")
        }
        return result.toString().dropLast(1) // Entferne das letzte ":"
    }

    fun ByteArray.reverse() : ByteArray {
        val reversedArray = ByteArray(this.size)

        for (i in this.indices) {
            reversedArray[i] = this[this.size - 1 - i]
        }

        return reversedArray
    }

    fun SettingsManager.readSetting(settingName: String, state: MutableState<String?>) {
        Log.d("readSetting","Start reading Setting ${settingName}")

        read(
            settingName,
            object : McuMgrCallback<McuMgrSettingsReadResponse> {
                override fun onResponse(response: McuMgrSettingsReadResponse) {
                    if (response.isSuccess) {
                        val byteArray = response.`val`
                        state.value = byteArray.toString(Charsets.UTF_8)
                        Log.d("readSetting", "onResponse called. Setting: $settingName, Response: ${state.value}")
                    }
                }

                override fun onError(p0: McuMgrException) {
                    Log.e("readSetting", "Could not read setting $settingName. Error: ${p0.message}")
                    Toast.makeText(
                        context,
                        "Konnte Setting \"$settingName\" nicht lesen: ${p0.message}, ${p0.cause}",
                        Toast.LENGTH_LONG
                    ).show()
                }
            })
    }

    fun SettingsManager.readSetting(settingName: String, settingState: MutableState<UInt>) {
            val callback = object : McuMgrCallback<McuMgrSettingsReadResponse> {
                override fun onResponse(response: McuMgrSettingsReadResponse) {
                    if (response.isSuccess) {
                        val byteArray = response.`val`
                        settingState.value = byteArray.toUInt()
                        Log.d("readSetting", "onResponse called. Setting: $settingName, Response: ${byteArray.toHex()}")
                    }
                }

                override fun onError(p0: McuMgrException) {
                    Log.e("readSetting", "Could not read setting $settingName. Error: ${p0.message}")
                    Toast.makeText(
                        context,
                        "Konnte Setting \"$settingName\" nicht lesen: ${p0.message}, ${p0.cause}",
                        Toast.LENGTH_LONG
                    ).show()
                }
            }

            // Read setting asynchronously
            read(settingName, callback)
        }

    suspend fun writeSetting(device: BluetoothDevice, settingName: String, value: String) =
        suspendCoroutine {cont->
            val transport = McuMgrBleTransport(context, device)
            val settingsManager = SettingsManager(transport)

            val callback = object : McuMgrCallback<McuMgrResponse> {
                override fun onResponse(response: McuMgrResponse) {
                    if (response.isSuccess) {
                        Log.d("writeSetting","Successfully written Setting $settingName")
                        cont.resume(null)
                    }
                }

                override fun onError(p0: McuMgrException) {
                    Log.e("writeSetting", "Failed for setting $settingName")
                    cont.resume(null)
                }
            }

            Log.d("Write Setting","try to write Setting $settingName with value \"${value.toByteArray().toHex()}\" with size ${value.toByteArray().size}")
            settingsManager.write(settingName, value.toByteArray(), callback)
        }

    suspend fun writeSetting(device: BluetoothDevice, settingName: String, value: UInt) =
        suspendCoroutine {cont->
            val transport = McuMgrBleTransport(context, device)
            val settingsManager = SettingsManager(transport)

            val callback = object : McuMgrCallback<McuMgrResponse> {
                override fun onResponse(response: McuMgrResponse) {
                    if (response.isSuccess) {
                        Log.d("writeSetting","Successfully written Setting $settingName")
                        cont.resume(null)
                    }
                }

                override fun onError(p0: McuMgrException) {
                    Log.e("writeSetting", "Failed for setting $settingName")
                    cont.resume(null)
                }
            }

            fun UInt.toByteArray(): ByteArray {
                val intVal = this.toInt()
                val byteArray = ByteArray(4)

                for (i in 0 until 4) {
                    byteArray[i] = ((intVal shr (8 * (3 - i))) and 0xFF).toByte()
                }

                return byteArray

            }

            Log.d("Write Setting","try to write Setting $settingName with value \"${value.toByteArray().toHex()}\" with size ${value.toByteArray().size}")
            settingsManager.write(settingName, value.toByteArray().reverse(), callback)
        }

    suspend fun saveSettings() =
        coroutineScope {
            val transport = McuMgrBleTransport(context, device)
            val settingsManager = SettingsManager(transport)

            val callback = object : McuMgrCallback<McuMgrResponse> {
                override fun onResponse(response: McuMgrResponse) {
                    if (response.isSuccess) {
                        Log.d("saveSettings","Successfully saved Settings")
                    }
                }

                override fun onError(p0: McuMgrException) {
                    Log.e("saveSettings", "Failed to save Settings")
                }
            }

            settingsManager.save(callback)

        }

    fun writeAllSettings() {
        CoroutineScope(Dispatchers.IO).launch {
            ssidState.value?.let { writeSetting(device, "wifi/ssid", it) }
            passwordState.value?.let { writeSetting(device, "wifi/password", it) }
            serverState.value?.let { writeSetting(device, "lwm2m/server", it) }
            techState.value?.let { writeSetting(device, "tech/imp_per_kwh", it) }

            saveSettings()
        }
    }

    fun ImageManager.getDeviceImages(state: MutableList<McuMgrImageStateResponse.ImageSlot>){
        val callback = object : McuMgrCallback<McuMgrImageStateResponse> {
            override fun onResponse(response: McuMgrImageStateResponse) {
                if (response.isSuccess) {
                    Log.d("getDeviceImages","Successfully read image information from device")
                    for (image in response.images.asList()!!){
                        state.add(image)
                    }
                }
            }

            override fun onError(p0: McuMgrException) {
                Log.e("getDeviceImages", "Failed to read Image information from device: ${p0.message}")
            }
        }

        list(callback)
    }

    suspend fun getDeviceImages() =
        suspendCoroutine { cont->
            val callback = object : McuMgrCallback<McuMgrImageStateResponse> {
                override fun onResponse(response: McuMgrImageStateResponse) {
                    if (response.isSuccess) {
                        Log.d("getDeviceImages","Successfully read all Device Images")
                        cont.resume(response.images.asList())
                    }
                }

                override fun onError(p0: McuMgrException) {
                    cont.resume(null)
                    Log.e("getDeviceImages", "Failed to read Image information from device: ${p0.message}")
                }
            }

            imageManager.list(callback)
        }

    fun readFileContent(uri: Uri): ByteArray? {
        val inputStream: InputStream? = context.contentResolver.openInputStream(uri)
        inputStream?.use { input ->
            val outputStream = ByteArrayOutputStream()
            input.copyTo(outputStream)
            return outputStream.toByteArray()
        }
        return null
    }

    fun getUpdateFileVersion(uri: Uri): String {
        val bytearray = readFileContent(uri)
        try {
            val mcuMgrImage = McuMgrImage.fromBytes(bytearray)
            uploadButtonEnabled.value = true
            return mcuMgrImage.header.version.toString()
        } catch (e: Exception) {
            // Behandlung der Ausnahme, z.B. Logging oder Benachrichtigung des Benutzers
            Log.e("Update", "Fehler beim Parsen des Bytearrays: ${e.message}", e)
            Toast.makeText(
                context,
                "Selected File is not a valid Firmware Update file!",
                Toast.LENGTH_LONG
            ).show()
            // Setze den Upload-Button auf deaktiviert, da das ByteArray nicht erfolgreich geparst werden konnte
            uploadButtonEnabled.value = false
        }

        return ""
    }

    LaunchedEffect(device){
        for (i in getDeviceImages()!!){
            if(i.active){
                image.value = i
            }
        }
        settingsManager.readSetting("lwm2m/device_type", devName)
        settingsManager.readSetting("lwm2m/manufacturer", devManufacturer)
        settingsManager.readSetting("lwm2m/hardware_version", devHardwareVersion)
        settingsManager.readSetting("wifi/ssid", ssidState)
        settingsManager.readSetting("wifi/password", passwordState)
        settingsManager.readSetting("lwm2m/server", serverState)
        settingsManager.readSetting("tech/imp_per_kwh", techState)

        isLoading.value = false
    }

    fun openFilePicker(context: android.content.Context, launcher: ActivityResultLauncher<Intent>) {
        val intent = Intent(Intent.ACTION_OPEN_DOCUMENT).apply {
            addCategory(Intent.CATEGORY_OPENABLE)
            type = "*/*"
            putExtra(Intent.EXTRA_MIME_TYPES, arrayOf("application/octet-stream", "application/signed-bin")) // MIME-Typen-Filterung für .signed.bin-Dateien
        }
        launcher.launch(intent)
    }

    @Composable
    fun FilePickerButton(onFileSelected: (Uri) -> Unit) {
        val filePickerLauncher = rememberLauncherForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == android.app.Activity.RESULT_OK) {
                result.data?.data?.let { uri ->
                    onFileSelected(uri)
                }
            }
        }

        Button(onClick = { openFilePicker(context, filePickerLauncher) }) {
            Text("Update auswählen")
        }
    }

    @Composable
    fun UploadButton(
        updateFile: MutableState<Uri?>,
        uploadButtonEnabled: MutableState<Boolean>
    ) {
        var fwupstate = remember { mutableStateOf<FwupState>(FwupState.NOT_SARTED) }
        var progress = remember { mutableFloatStateOf(0f) }

        var callback = object : FirmwareUpgradeCallback {
            override fun onUpgradeStarted(p0: FirmwareUpgradeController?) {

            }

            override fun onUploadProgressChanged(p0: Int, p1: Int, p2: Long) {
                progress.floatValue = p0.toFloat() / p1.toFloat()
                Log.d("Updater", "Progress: ${progress.floatValue}")
            }

            override fun onStateChanged(
                p0: FirmwareUpgradeManager.State?,
                p1: FirmwareUpgradeManager.State?
            ) {
                Log.d("Updater", "State changed from $p0 to $p1")
                if (p1 == FirmwareUpgradeManager.State.RESET){
                    fwupstate.value = FwupState.REBOOTING
                }
            }

            override fun onUpgradeCompleted() {
                Log.d("Updater", "Completed Update")
                fwupstate.value = FwupState.FINISHED
            }

            override fun onUpgradeCanceled(p0: FirmwareUpgradeManager.State?) {
                Log.d("Updater", "Cancel Update")
                fwupstate.value = FwupState.FINISHED
            }

            override fun onUpgradeFailed(
                p0: FirmwareUpgradeManager.State?,
                p1: McuMgrException?
            ) {
                Log.d("Updater", "Update failed: ${p1?.message}")
                Toast.makeText(context, "Upload failed because: ${p1?.message}", Toast.LENGTH_LONG).show()
                fwupstate.value = FwupState.FAILED
            }
        }
        val dfu = FirmwareUpgradeManager(imageManager.transporter, callback)
        dfu.setEstimatedSwapTime(60000)
        dfu.setMode(FirmwareUpgradeManager.Mode.TEST_ONLY)

        Button(
            onClick = {
                val uri = updateFile.value
                if (uri != null) {
                    fwupstate.value = FwupState.UPLOADING
                    progress.floatValue = 0f

                    val image = readFileContent(uri)
                    image?.let { dfu.start(it) }
                }
            },
            enabled = uploadButtonEnabled.value
        ){
            Text ("Upload")
            if(fwupstate.value != FwupState.NOT_SARTED){
                showUploadDialog(fwupstate, progress, dfu)
            }
        }
    }

    @Composable
    fun displayDeviceInformation(){
        Card(modifier = Modifier.fillMaxWidth(), colors = CardDefaults.cardColors(containerColor = MaterialTheme.colorScheme.background)) {
            Column(modifier = Modifier.padding(all=5.dp)) {
                Text("Device Information", style = MaterialTheme.typography.titleLarge)
                Row(modifier = Modifier.fillMaxWidth(0.5f), horizontalArrangement = Arrangement.SpaceBetween){
                    Text("Device Type:", style = MaterialTheme.typography.bodyMedium)
                    Text("${devName.value ?: "Lade..."}", style = MaterialTheme.typography.bodyMedium)
                }
                Row(modifier = Modifier.fillMaxWidth(0.5f), horizontalArrangement = Arrangement.SpaceBetween){
                    Text("Manufacturer:", style = MaterialTheme.typography.bodyMedium)
                    Text("${devManufacturer.value ?: "Lade..."}", style = MaterialTheme.typography.bodyMedium)
                }
                Row(modifier = Modifier.fillMaxWidth(0.5f), horizontalArrangement = Arrangement.SpaceBetween){
                    Text("Hardware Version:", style = MaterialTheme.typography.bodyMedium)
                    Text("${devHardwareVersion.value ?: "Lade..."}", style = MaterialTheme.typography.bodyMedium)
                }
                Row(modifier = Modifier.fillMaxWidth(0.5f), horizontalArrangement = Arrangement.SpaceBetween){
                    Text("Firmware Version:", style = MaterialTheme.typography.bodyMedium)
                    Text("${image.value?.version ?: "Lade..."}", style = MaterialTheme.typography.bodyMedium)
                }
            }
        }
    }

    @Composable
    fun displaySettings(){
        Card(modifier = Modifier.fillMaxWidth(), colors = CardDefaults.cardColors(containerColor = MaterialTheme.colorScheme.background)) {
            Column(modifier = Modifier.padding(all=5.dp)) {
                Text("Settings", style = MaterialTheme.typography.titleLarge)
                OutlinedTextField(
                    value = "${ssidState.value ?: "Lade..."}",
                    onValueChange = { ssidState.value = it },
                    label = { Text(text = "Wifi SSID") }
                )
                OutlinedTextField(
                    value = "${passwordState.value ?: "Lade..."}",
                    onValueChange = { passwordState.value = it },
                    label = { Text(text = "Wifi Passwort") },
                    visualTransformation = PasswordVisualTransformation(),
                    keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Password)
                )
                OutlinedTextField(
                    value = "${serverState.value ?: "Lade..."}",
                    onValueChange = { serverState.value = it },
                    label = { Text(text = "LWM2M Server Adresse") }
                )
                OutlinedTextField(
                    value = "${techState.value ?: "Lade..."}",
                    onValueChange = { if(!it.isEmpty()) techState.value = it.toUInt() },
                    singleLine = true,
                    keyboardOptions = KeyboardOptions.Default.copy(
                        keyboardType = KeyboardType.Number,
                    ),
                    visualTransformation = VisualTransformation.None,
                    label = { Text(text = "Imp/kWh") }
                )
                Spacer(modifier = Modifier.weight(1f))
                Button(onClick = {
                    writeAllSettings()
                }) {
                    Text(text = "Speichern")
                }
            }
        }
    }

    @Composable
    fun displayImage(image: McuMgrImageStateResponse.ImageSlot){
        Card(modifier = Modifier.fillMaxWidth(), colors = CardDefaults.cardColors(containerColor = MaterialTheme.colorScheme.background)){
            Column(modifier = Modifier.padding(all=5.dp)) {
                Text("Image Slot ${image.slot}", style = MaterialTheme.typography.titleLarge)
                Row(){
                    Text("Version:", modifier= Modifier.weight(0.25f), style = MaterialTheme.typography.bodyMedium)
                    Text("${image.version}", modifier= Modifier.weight(0.75f), style = MaterialTheme.typography.bodyMedium)
                }
                Row(){
                    Text("Hash:", modifier= Modifier.weight(0.25f), style = MaterialTheme.typography.bodyMedium)
                    Text("${image.hash.toHex()}", modifier= Modifier.weight(0.75f), style = MaterialTheme.typography.bodyMedium)
                }
                Row(){
                    Text("Active:", modifier= Modifier.weight(0.25f), style = MaterialTheme.typography.bodyMedium)
                    Text("${image.active}", modifier= Modifier.weight(0.75f), style = MaterialTheme.typography.bodyMedium)
                }
                Row(){
                    Text("Confirmed:", modifier= Modifier.weight(0.25f),style = MaterialTheme.typography.bodyMedium)
                    Text("${image.confirmed}", modifier= Modifier.weight(0.75f), style = MaterialTheme.typography.bodyMedium)
                }
                Row(){
                    Text("Permanent:", modifier= Modifier.weight(0.25f), style = MaterialTheme.typography.bodyMedium)
                    Text("${image.permanent}", modifier= Modifier.weight(0.75f), style = MaterialTheme.typography.bodyMedium)
                }
            }
        }
    }

    @Composable
    fun displayFirmwareUpdate(){
        Card(modifier = Modifier.fillMaxWidth(), colors = CardDefaults.cardColors(containerColor = MaterialTheme.colorScheme.background)) {
            Column(modifier = Modifier.padding(all=5.dp) ){
                Text("Firmware Update", style = MaterialTheme.typography.titleLarge)
                Row(modifier = Modifier.fillMaxWidth(0.75f), horizontalArrangement = Arrangement.SpaceBetween){
                    Text("Version:", style = MaterialTheme.typography.bodyMedium)
                    Text(updateFileVersion.value ?: "Select a Firmware Update File", style = MaterialTheme.typography.bodyMedium)
                }
                Row(modifier = Modifier.padding(all=5.dp)) {
                    FilePickerButton { uri ->
                        updateFile.value = uri
                        updateFileVersion.value = getUpdateFileVersion(uri)
                    }
                    UploadButton(updateFile, uploadButtonEnabled)
                }
            }
        }
    }

    TestTheme {
        Surface(
            modifier = Modifier.fillMaxSize(),
            color = MaterialTheme.colorScheme.surface
        ) {
            if (device != null) {
                Column(
                    modifier = Modifier.fillMaxWidth(),
                    verticalArrangement = Arrangement.spacedBy(5.dp)
                ) {
                    TopAppBar(
                        title = {
                            Column(
                                modifier = Modifier.weight(1f),
                                verticalArrangement = Arrangement.Center,
                            )
                            {
                                Text(
                                    text = "${device.name}",
                                    style = MaterialTheme.typography.titleLarge
                                )
                                Text(
                                    text = "${device.address}",
                                    style = MaterialTheme.typography.bodyMedium
                                )
                            }
                        },
                        colors = TopAppBarDefaults.topAppBarColors(
                            containerColor = MaterialTheme.colorScheme.primary,
                            titleContentColor = MaterialTheme.colorScheme.onPrimary,
                        ),
                        navigationIcon = {
                            IconButton(onClick = { navController.popBackStack() }) {
                                Icon(
                                    imageVector = Icons.AutoMirrored.Filled.ArrowBack,
                                    tint = MaterialTheme.colorScheme.onPrimary,
                                    contentDescription = "Localized description"
                                )
                            }
                        }
                    )
                    if (isLoading.value) {
                        LinearProgressIndicator(modifier = Modifier.fillMaxWidth())
                    } else {
                        Column(
                            modifier = Modifier
                                .padding(all = 5.dp)
                                .verticalScroll(rememberScrollState())
                        ) {
                            displayDeviceInformation()
                            HorizontalDivider(
                                modifier = Modifier.fillMaxWidth(),
                                thickness = 1.dp,
                                color = MaterialTheme.colorScheme.onSurface
                            )
                            displaySettings()
                            HorizontalDivider(
                                modifier = Modifier.fillMaxWidth(),
                                thickness = 1.dp,
                                color = MaterialTheme.colorScheme.onSurface
                            )
                            displayFirmwareUpdate()
                        }
                    }
                }
            } else {
                Text(text = "Kein Gerät ausgewählt")
            }
        }
    }
}
